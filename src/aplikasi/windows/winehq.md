# WineHQ

## Deskripsi

[WineHQ] adalah lapisan kompatibilitas yang mampu menjalankan perangkat lunak Windows pada beberapa sistem operasi yang sesuai dengan POSIX, seperti Linux, macOS, & BSD. Cara kerjanya yaitu dengan mensimulasikan logika Windows internal seperti mesin virtual atau emulator, Wine menerjemahkan panggilan API Windows ke panggilan POSIX saat itu juga, menghilangkan penalti kinerja dan memori dari metode lain dan memungkinkan pengguna untuk mengintegrasikan perangkat lunak Windows ke desktop dengan rapi.

## Cara memasang

Memasang wine beserta aplikasi pendukung lainnya.

```
get protontricks wine wine-gecko \
    wine-mono wine-tools winetricks
```

Jika membutuhkan wine untuk arsitektur 32-bit (khusus `x86_64` saja), maka pasang repositori multilib terlebih dahulu.

```
get void-repo-multilib
```

Memasang wine 32bit.

```
get wine-32bit SDL2-32bit alsa-lib-32bit \
    alsa-plugins-32bit attr-32bit gettext-32bit \
    giflib-32bit gnutls-32bit gst-plugins-base1-32bit \
    gtk+3-32bit libXcomposite-32bit libnl3-32bit \
    libpcap-32bit libpulseaudio-32bit libunwind-32bit \
    libva-32bit ocl-icd-32bit sqlite-32bit \
    v4l-utils-32bit vulkan-loader-32bit
```

Tunggu sampai proses selesai.

[WineHQ]:https://www.winehq.org/
